import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
create_inventory_table =  "CREATE TABLE Inventory (username TEXT, product TEXT, in_inventory INT CHECK(in_inventory >= 0));"
player__inventory_total_request = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"
add_to_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"


def get_connection():
    conn = psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="Alohomora41",
        host="0.0.0.0",
        port=5432
    )
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE)  # TODO add your values here
    return conn

def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)

                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(player__inventory_total_request, obj)
                inventory_total = cur.fetchone()[0] or 0
                
                if inventory_total + amount > 100:
                    raise Exception("Inventory limit for products is reached")

                cur.execute(add_to_inventory , obj)
                if cur.rowcount != 1:
                    raise Exception("Failed to update inventory")
            except Exception as e:
                conn.rollback()
                raise e




buy_product('Alice', 'marshmello', 1000)