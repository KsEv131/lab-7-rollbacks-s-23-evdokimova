## In order to run
- `docker compose up -d`

- `docker exec -it postgres_container psql -U postgres -f init.sql`

- `pip install -r requirements.txt`

- `python3 buy_product.py`

## Part 1 (2/3 points)
To exclude data inconsistency, I deleted excessive "with" part.
In order to run

## Part 2 (3/3 points)
I have extended the functionality of the provided function by adding Inventory (table `Inventory` which stores username, product name and amount of this product with limit of 100 producrts for one player).
Also, in order to exclude some anomalies I have changed predifined by psycopg2 isolation level from REPEATABLE READ (where dirty reads are not possible. Write locks and read locks are maintained.) to SERIALIZABLE (where dirty reads, nonrepeatable reads, phantom reads, and serialization anomalies are not possible. Statements for read, write, and ranges are locked.).


## Extra part (1.5 points)
If everything is commited, but the function still fails (e.g. electricity shortage, client-server connection broken or something else), some clients might try to retry the request.
Nevertheless, in order to maintain consistency in a database, before and after the transaction, ACID properties are followed. One of them - Durability - states that even if the system failure occurs, the changes of a successful transaction occur.
For example, due to this, when a client retries the request, the situation with 2 added entities can arise.
To avoid wrong results, we will ensure that such requests are idempotent. 
before applying the `buy_product` operation by creating an unique key for each executed transaction. The used keys will be saved into  created table:
```sql
CREATE TABLE received_transaction_keys
(
    used_key VARCHAR(256) UNIQUE
)
```
Then, before the next transatsion execution, we just will check that its key is not present yet.  
```python
k = conn.fetchone("SELECT 1 FROM received_transaction_keys WHERE used_key=%(used_key)s")
if not k:
    # buy_product with the provided parameters executed
    buy_product(...) 
```
If it isn't - this key will be added to keys table.

### Extra part (1.5 points)
We can work with several databases or with some external REST CRUD services or message queues, so we need to make them cooperating.
To resolve such problem, we can use a **two-phase commit** protocol, that ensures that a database commit is done properly (atomic) over multiple resources. So coordinator completes the transaction if it receives an acknowledgement from all the participants.
